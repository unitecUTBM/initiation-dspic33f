#include "uart.h"

#include <xc.h>
#include <p33Fxxxx.h>
#include <stdlib.h>
#include <libpic30.h>

#include "system.h"

void uart_init(){
    /* Clock config */
    U1MODEbits.ABAUD = 0;       // disable baudrate detection
    U1MODEbits.BRGH = 0;        // 16x baud clock, High-Speed mode

    /* uart serttings */
    U1BRG = (unsigned int)42;  // 57600 baud with FCY=40MHz 
    U1MODEbits.STSEL = 0;       // 1 stop bit
    U1MODEbits.PDSEL = 0;       //no parity

    /* Peripheral Pin Select */
    RPOR12bits.RP24R = 0b00011; // U1TX - RPn tied to UART1 Transmit
    RPINR18bits.U1RXR = 25;     // U1RX - RPn tied to UART1 Receive
    
    /* enable module and pins */
    U1MODEbits.UEN = 0b00;      // U1TX and U1RX pins are enabled and used;
                                // U1RTS and U1CTS pin controlled by port latches

    U1MODEbits.LPBACK = 0b0; // Loopback mode is disabled

    U1MODEbits.UARTEN = 1;      // UART1 is enabled
    U1STAbits.UTXEN = 1;        // Transmit enabled, U1TX pin controlled by UART1


}
