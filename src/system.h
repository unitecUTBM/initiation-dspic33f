/*
 * File:   system.h
 * Author: arthur
 *
 * Created on January 29, 2019, 9:20 PM
 */

#ifndef SYSTEM_H
#define SYSTEM_H

// Réglage du processeur
// Configuration de l'oscillateur pour fonctionnement proche des 40 Mips
// Fosc = FQ*M/(N1*N2), Fcy = Fosc/2
#define _FQ 10e6 // Fréquence du quartz sur carte (Hz)
#define _M 32    // Facteur M de la PLL
#define _N1 2    // Diviseur N1
#define _N2 2    // Diviseur N2

// Calcul du réglage de l'horloge
#define PLLREG_PLLFBD (_M - 2)   // Registre du facteur M de la PLL
#define PLLREG_PLLPOST (_N1 - 2) // Registre du post diviseur N1 de la PLL
#define PLLREG_PLLPRE (_N2/2 - 1)  // Registre du pré diviseur N2 de la PLL

#define FCY                                                                    \
    ((float)_FQ * _M / (2 * _N1 * _N2)) // Fréquence CPU (Calcul) = 40MHz

void DSP_init();

#endif /* SYSTEM_H */
