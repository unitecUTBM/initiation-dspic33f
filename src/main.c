/*
 * File:   main.c
 * Author: aklipfel
 *
 * Created on 17 janvier 2019, 10:06
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "system.h"
#include "uart.h"

#include <libpic30.h>
#include <p33Fxxxx.h>
#include <xc.h>

// FOSCSEL
#pragma config FNOSC = PRIPLL
// FOSC
#pragma config POSCMD = XT
#pragma config OSCIOFNC = OFF
#pragma config FCKSM = CSDCMD

#pragma config FWDTEN = OFF // Watchdog Timer Enable (Watchdog timer
                            // enabled/disabled by user software)
#pragma config ICS = \
    PGD3                    // Comm Channel Select (Communicate on PGC3/EMUC3 and PGD3/EMUD3)
#pragma config JTAGEN = OFF // JTAG Port Enable (JTAG is Disabled)

/*
 *
 */
int main(int argc, char **argv)
{
    TRISAbits.TRISA4 = 0;
    TRISBbits.TRISB10 = 0;
    PORTAbits.RA4 = 1;
    PORTBbits.RB10 = 1;

    DSP_init();

    __delay_ms(100);

    uart_init();

    __delay_ms(100);

    int i;
    for (i = 0;; i++)
    {
        __delay_ms(500);
        printf("test %d\r\n", i);
        PORTAbits.RA4 = 0;
        PORTBbits.RB10 = 0;
        __delay_ms(500);
        PORTAbits.RA4 = 1;
        PORTBbits.RB10 = 1;
    }

    return 0;
}
