#include "system.h"

#include <xc.h>

void DSP_init() {
    // Desactivation du WatchDog
    RCONbits.SWDTEN = 0;

    // Configuration de l'oscillator pour fonctionnement ? 40Mips
    // Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
    // Fosc= 8M*40/(2*2)=80Mhz pour 8M en horloge d'entr?e
    PLLFBD = PLLREG_PLLFBD;
    CLKDIVbits.PLLPOST = PLLREG_PLLPOST;
    CLKDIVbits.PLLPRE = PLLREG_PLLPRE;

    // Clock switching to incorporate PLL
    __builtin_write_OSCCONH(0x03); // Initiate Clock Switch to Primary
    // Oscillator with PLL (NOSC=0b011)
    __builtin_write_OSCCONL(0x01); // Start clock switching

    while (OSCCONbits.COSC != 0b011)
        ; // Wait for Clock switch to occur
    // Wait for PLL to lock
    while (OSCCONbits.LOCK != 1)
        ;
}
