
GXX = xc16-gcc
BIN2HEX = xc16-bin2hex

# source directory
SRC_DIR = src
# object directory
OBJ_DIR = obj
# .elf and .hex output
PROD_DIR = prod
# debug directory
DEBUG_DIR = debug
# release directory
RELEASE_DIR = release

#list of all .h files
HEADER = $(wildcard $(SRC_DIR)/*.h)
#list of all .c files
SRC = $(wildcard $(SRC_DIR)/*.c)
#list of all .s files
SRC_ASM = $(wildcard $(SRC_DIR)/*.s)
#list of all .h files
SRC_PROTO = $(wildcard $(SRC_DIR)/*.proto)
#list of all .o files for release build
OBJ := $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/$(RELEASE_DIR)/%.o,$(wildcard $(SRC_DIR)/*.c)) $(patsubst $(SRC_DIR)/%.s,$(OBJ_DIR)/$(RELEASE_DIR)/%.o,$(wildcard $(SRC_DIR)/*.s))
#list of all .o files for debug build
OBJ_DEBUG := $(patsubst $(SRC_DIR)/%.c,$(OBJ_DIR)/$(DEBUG_DIR)/%.o,$(wildcard $(SRC_DIR)/*.c)) $(patsubst $(SRC_DIR)/%.s,$(OBJ_DIR)/$(DEBUG_DIR)/%.o,$(wildcard $(SRC_DIR)/*.s))
# output
OUTPUT_NAME = tp0
OUTPUT_HEX = $(OUTPUT_NAME).hex
OUTPUT = $(OUTPUT_NAME).elf

LIBS = 
# uc is a dsPIC33FJ128MC804
TARGET = 33FJ128MC804
# include file from source directory
INCLUDES = -I$(SRC_DIR)
# some flags
CFLAGS = -Wall -Werror -legacy-libc -omf=elf
#microchip's madness highlited (linking option)
XC16MADNESS = -Wl,--defsym=__MPLAB_BUILD=1,--script=p$(TARGET).gld,--heap=512,--stack=512,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io

PWD = $(shell pwd)

# Detect os
MPLAP_UPLOAD 				:=
ifeq ($(OS),Windows_NT)
	MPLAP_UPLOAD += @echo "Upload not suported on windows" \#
else
	UNAME_S := $(shell uname -s)
	ifeq ($(UNAME_S),Linux)
		MPLAP_UPLOAD += java -jar /opt/microchip/mplabx/v5.15/mplab_platform/mplab_ipe/ipecmd.jar
	endif
	ifeq ($(UNAME_S),Darwin)
		MPLAP_UPLOAD += /Applications/microchip/mplabx/v5.15/mplab_platform/mplab_ipe/bin/ipecmd.sh
	endif
endif

# build
build: build-directory hex-build

# .hex file generation pattern
hex-build: $(PROD_DIR)/$(RELEASE_DIR)/$(OUTPUT)
	$(BIN2HEX) $< -omf=elf

# .elf file generation pattern
$(PROD_DIR)/$(RELEASE_DIR)/$(OUTPUT): $(OBJ) 
	$(GXX) $(CFLAGS) -mcpu=$(TARGET) $(INCLUDES) $^ $(LIBS) -o $@ $(XC16MADNESS)

# .o file generation pattern (from .c file)
$(OBJ_DIR)/$(RELEASE_DIR)/%.o : $(SRC_DIR)/%.c
	$(GXX) $(CFLAGS) -mcpu=$(TARGET) $(INCLUDES) -c $< -o $@

# .o file generation pattern (from .s file)
$(OBJ_DIR)/$(RELEASE_DIR)/%.o : $(SRC_DIR)/%.s
	$(GXX) $(CFLAGS) -mcpu=$(TARGET) $(INCLUDES) -c $< -o $@

# clean and build
all: clean build upload

upload:
	$(MPLAP_UPLOAD) -#5 -P$(TARGET) -TPPK3 -OL -M -F$(PWD)/$(PROD_DIR)/$(RELEASE_DIR)/$(OUTPUT_HEX)

# directory generation pattern
build-directory: $(OBJ_DIR)/$(DEBUG_DIR) $(OBJ_DIR)/$(RELEASE_DIR) $(PROD_DIR)/$(DEBUG_DIR) $(PROD_DIR)/$(RELEASE_DIR)

$(OBJ_DIR)/$(DEBUG_DIR):
	@mkdir -p $@

$(OBJ_DIR)/$(RELEASE_DIR):
	@mkdir -p $@

$(PROD_DIR)/$(DEBUG_DIR):
	@mkdir -p $@

$(PROD_DIR)/$(RELEASE_DIR):
	@mkdir -p $@

# clean
clean:
	rm -rf $(PROD_DIR) $(OBJ_DIR) log.* MPLABXLog.xml*

# build debug
build-debug: build-directory hex-build-debug

# .hex file generation pattern
hex-build-debug: $(PROD_DIR)/$(DEBUG_DIR)/$(OUTPUT)
	$(BIN2HEX) $< -omf=elf

# .elf file generation pattern
$(PROD_DIR)/$(DEBUG_DIR)/$(OUTPUT): $(OBJ_DEBUG)
	$(GXX) $(CFLAGS) -g -mcpu=$(TARGET) $(INCLUDES) $^ $(LIBS) -o $@ $(XC16MADNESS)

# .o file generation pattern (from .c file)
$(OBJ_DIR)/$(DEBUG_DIR)/%.o : $(SRC_DIR)/%.c
	$(GXX) $(CFLAGS) -g -mcpu=$(TARGET) $(INCLUDES) -c $< -o $@

# .o file generation pattern (from .s file)
$(OBJ_DIR)/$(DEBUG_DIR)/%.o : $(SRC_DIR)/%.s
	$(GXX) $(CFLAGS) -g -mcpu=$(TARGET) $(INCLUDES) -c $< -o $@