# Guide de survie en milieu Microchip
*première prise en main d'un dsPIC33F*

__Prérequis :__
* ce tutoriel suppose que vous avez des connaissances basique sur le langage C

Le tutoriel est fait pour fonctionner avec VSCode, mais il est toujours possible d'utiliser MPLabX. Aussi, le projet fonctionne sur Windows, Linux et Mac. Cependant, l'installation de tout les logiciels ne sera pas expliqué pour Mac.

__Objectifs :__
* Test de la tool-chain de Microchip
* Découvert de la programmation sur un microcontrôleur
* Apprendre à lire une Datasheet

## Les IDE et les tool-chains

* IDE: Environement de développement, un IDE est un logiciel qui permet de programmer. Il dispose au minimum d'un éditeur de fichier mais il regroupe généralement plusieurs outils comme le compilateur, le débugger, etc.
* tool-chains: C'est l'ensemble des outils qui servent à compiler un programme

### Installation

#### Installation minimale

Pour pouvoir utiliser les microcontrôleurs PIC de Microchip, il faut au minimum deux choses:
* MPlabX: L'ide de microchip, il comporte un étideur de texte, il est capable de programmer les PIC avec un ICD ou un pickit (deux boîtiers qui permettent de connecter un ordinateur avec le PIC). MPLabX comporte aussi un debugger et peut configurer des projets et les compilers
* XC16: Le compilateur pour les PIC 16 bits de Microchip (PIC24/dsPIC30/dsPIC33). Le compilateur n'est pas directement inclue avec MPLabX et doit être installé manuellement. Cependant, MPLabX est capable de détecter et d'utiliser le compilateur automatiquement s'il est correctement installer.
* Peripheral library: bibliohtèque qui permet de simplifier la programmation (notament l'utilisation des registres)

#### Logiciels additionnels

Pour ce tutoriel, certain logiciel additionnelle sont tout de même nécéssaire:
* make: outils qui permet de compiler le projet à partir d'un `Makefile`
* Logiciel pour ouvrir un port série: Vous pouvez par exemple utiliser Putty (l'utilisation sera expliqué au cours du tutoriel) mais n'importe quel logiciel fera l'affaire.
* Visual Studio Code: Personnellement, je n'aime pas MPLabX, c'est pourquoi ce tuto est prévu pour fonctionner sous VSCode. Mais il est toujours possible l'utiliser MPLabX

### Installation des logiciels Microchip

#### Prérequis sur linux

Pour que MPLabX fonctionne, il faut installer des bibliothèques 32 bits pour Linux. La ligne ci-dessous permet d'installer les dépendances pour Ubuntu, mais il est possible de les installer sur toutes les distributions:
```shell
sudo apt-get install libc6:i386 libx11-6:i386 libxext6:i386 libstdc++6:i386 libexpat1:i386
```

#### Télécharger et installer les logiciels

Tous les logiciels sont disponibles pour Windows, Linux et Mac:
* [MPLabX](https://www.microchip.com/mplab/mplab-x-ide)
* [XC16](https://www.microchip.com/mplab/compilers)
* [Peripheral library](https://www.microchip.com/SWLibraryWeb/product.aspx?product=PIC24%20MCU%20dsPIC%20Peripheral%20Lib)

Il suffit de télécharger les logiciels pour votre OS et les installer. Attention, vérifier bien que vous avez installer XC16 et pas un autre compilateur comme XC8 ou XC32. Quand vous installez la bibliothèque des périphériques, fait attention de l'installer dans le bon dossier, l'emplacment par défaut n'est pas juste.

### Les autres logiciels

#### Putty
* Windows: [Site officiel de Putty](https://www.putty.org/)
* Linux: [Doc de putty pour Linux](https://doc.ubuntu-fr.org/putty) Il est aussi possible d'utiliser d'autres outils comme screen ou minicom.

#### make
* Windows: [GNU make](http://gnuwin32.sourceforge.net/packages/make.htm)
* Linux:
```
sudo apt-get install make
```

### Visual Studio Code

Installation: [Visual Studio Code](https://code.visualstudio.com/)

#### Configuration pour Windows
Exemple de configuration du compilateur avec visual studio sur Windws (`.vscode/c_cpp_propoerties.json`):
```json
{
    "configurations": [
        {
            "name": "microchip XC16",
            "includePath": [
                "${workspaceFolder}/**",
                "C:/Program Files (x86)/microchip/xc16/v1.36/include/**",
                "C:/Program Files (x86)/microchip/xc16/v1.36/support/dsPIC33F/**",
                "C:/Program Files (x86)/microchip/xc16/v1.36/support/generic/**"
            ],
            "defines": [
                "__dsPIC33FJ128MC804__"
            ],
            "compilerPath": "C:/Program Files (x86)/microchip/xc16/v1.36/bin/xc16-gcc",
            "cStandard": "c99"
        }
    ],
    "version": 4
}
```

#### Configuration pour Linux
Exemple de configuration du compilateur avec visual studio sur Linux (`.vscode/c_cpp_propoerties.json`):
```json
{
    "configurations": [
        {
            "name": "microchip XC16",
            "includePath": [
                "${workspaceFolder}/**",
                "/opt/microchip/xc16/v1.36/include/**",
                "/opt/microchip/xc16/v1.36/support/dsPIC33F/**",
                "/opt/microchip/xc16/v1.36/support/generic/**"
            ],
            "defines": [
                "__dsPIC33FJ128MC804__"
            ],
            "compilerPath": "/opt/microchip/xc16/v1.36/bin/xc16-gcc",
            "cStandard": "c99"
        }
    ],
    "version": 4
}
```

## Code d'example

Le code d'exemple fourni permet de tester les fonctionnalisées basique de la carte d'Unitec. Elle permet notamment de faire clignoter une led et envoyer des données sur un port série (avec des printf). Ces deux fonctionnalités vont nous permettre de prendre en main le microcontrôleur et d'expérimenter avec les configurations de bases qui lui permettent de fonctionner normalement.

### Ouvrir le projet

* VSCode: `File > Open Folder...`
* MPLabX: `File > Open Project...`

### Makefile

Pour compiler et uploader le code source sur le microcontrôleur, on utilise make. Make est un outils qui permet de définir des scripts, c'est un outil pensé pour facilité la compilation, mais il peut dépendant autre utilisé à d'autres fins. L'idée derrière cet outil et d'exécuter une série de commandes complexe avec une seul commande simple, la compilation est donc beaucoup plus simple avec un make. L'ensemble des scripts se trouve dans le fichier `Makefile` présent à la racine du projet. L'écriture d'un Makefile n'est pas évidente, mais une fois terminé, c'est un gain de temps énorme.

L'utilisation du makefile est simple :
* pour compiler tout le projet, on utilisera la commande `make build`
* pour envoyer le code compilé sur le microcontrôleur avec un pickit3, on utilisera la commande `make upload`

Pour plus de précision sur les Makefiles et la tool-chain de Mircochip, rendez vous sur la documentation du docker de compilation d'Unitec pour XC16: [compilation-xc16-et-docker](https://gitlab.com/unitecUTBM/compilation-xc16-et-docker)

Pour MPLabX, il ets possible de compiler et envoyer le programme `Make and Program Device`

### Premier test

Tester le code d'exemple en ouvrant un terminal dans le projet et exécutez les commandes `make build` et `make upload`. Une fois terminé, une led de la carte devrait clignoter une fois par seconde.

Il est maintenant temps de tester la liaison série. La liaison série (module UART sur le dsPIC33F) est un bus de communication basique présent sur tout les microcontrôleur qui permet de communiquer "simplement" comme on pourrait le faire en langage C avec les fonctions `printf` et `scanf` ou avec `Serial.print` et `Serial.read` sur l'IDE Arduino.

Il faut déjà trouver le port série sur lequel est connecté le circuit imprimé. Sur Windows ouvrez le gestionnaire des périphériques et recherchez un périphérique qui devrait commencer par "COM" suivit d'un nombre. Sur Linux, recherchez le port série avec la commande `ls /dev`, le port série devrait avoir un nom du type `/dev/ttyUSB` ou `/dev/ttyS`. Ensuite, si vous utilisez putty pour communiquer sur le port série, ouvrez putty, sélectionner l'option "Serial", entre le nom du port série (par exemple "COM3" ou "/dev/ttyUSB0") et pour le baudrate, entrez 57600 (c'est la vitesse de communication.). Ouvrez le port série et vous devriez voir le terminal se remplir au fur à mesure de ligne commençant par "test" suivie d'un chiffre qui augmente à chaque ligne. Si c'est effectivement le cas, bravo ! Vous avez programmé votre premier PIC avec succès !

Vous pouvez maintenant jouer avec le fichier main.c 'dans le dossier `src` du projet) Pour modifier le comportement du programme. Essayez de modifier la ligne est afficher le texte sur la liaison série et la fréquence de clignotement de la led par exemple.

## Expérimentation

### Datasheet

Tout d'abord, il faut apprendre à lire la documentation technique du microcontrôleur pour être capable de l'utiliser. (Disponible ici : [Datasheet du dsPIC33JF128MC804](http://ww1.microchip.com/downloads/en/devicedoc/70291g.pdf))

Les datasheets sont généralement toutes structuré de la même façon. Les premières pages décrivent les fonctionnalités principales du microcontrôleur. Ensuite, la documentation détaille le fonctionnement de chaque module du microcontrôleur séparément. Prenons l'exemple du module pour la gestion des entrées/sorties (I/O PORTS page 163) On retrouve une description global du fonctionnement du PIC ainsi qu'un schéma logique qui représente le fonctionnement interne des entrées/sorties. Il n'est généralement pas nécessaire de lire la totalité du Datasheet, ni de comprendre complètement le fonctionnement du microcontrôleur pour s'en servir. Il suffit généralement de piocher les bonnes informations et d'être capable de s'y retrouver dans un document technique qui fait généralement plusieurs centaines de pages et qui est destiné à des professionnelles (oui, c'est très difficile, mais ça s'apprend.).

Dans notre cas, il suffit de lire la première page pour se faire une idée du fonctionnement des entrées/sorties. Pour utiliser les entrées/sorties (ou I/O) il faudra utiliser un autre document [DS70193](http://ww1.microchip.com/downloads/en/DeviceDoc/70193c.pdf) pour expliquer en détail comment se servir des registres pour faire fonctionner les I/O. Les explications se trouvent page 2 et 3.

### Utiliser d'autres entrées/sorties

Essayez de faire clignoter une autre led de la carte à la place de la led actuelle. Pour se faire, itendifiez le pin qui contrôle la led (page 6 du datasheet du PIC), puis editer le registre TRIS associé pour mettre ce pin en sortie (inspirez vous du code déjà existant). Il est maintenant possible de contrôler la led avec le registre PORT du pin. Attention, il existe plusieurs port du le dsPIC33F, notament le porte A,B et C, vous devez bien identifier le port associer au pin que vous voulez utiliser.

### Changer la configuration du port série

Dans le fichier `uart.c` du dossier `src` le registre `U1BRG` est édité pour configurer la vitesse de transmission ligne 16. La configuration permet de communiquer à 9600 baud pour une horloge système qui tourne à 40MHz. Changez la vitesse de transmission pour la mettre à 57600 baud, vous trouverez les informations dans la documentation technique détaillé sur le module UART page 10 [DS70188](http://ww1.microchip.com/downloads/en/devicedoc/70188e.pdf).
Vérifiez que le baudrate est bien configurer en ouvrant le port série.

### Changer le configuration de l'horloge système

Changer la fréquence de l'horloge du système pour la faire fonctionner à 20MHz plutôt qu'à 40MHz. Pour vous aider, allez à la page 143 du datasheet du PIC et utiliser les macros définies dans le fichier `system.h` dans le répertoire `src` du projet. Les constants, qui permettent d'éditer la configuration, sont `_M`, `_N1` et `_N2` de la ligne 15 à la ligne 17.

Pour tester, reconfigurez le baudrate du module UART pour qu'il fonctionne correctement avec la nouvelle fréquence d'horloge.

## Résulat

Toujours vivant ?
Bravo, vous devriez maintenant mieux comprendre la programmation sur microcontrôleur.

Donc même si c'est difficile au début, la majorité du temps, on n'aura pas besoin d'utiliser directement les registres, la programmation sera donc beaucoup moins pénible grâce à la création de fonction pour manipuler les registres à notre place. Oui, c'est normal, c'est évident pour personne. Mais rassurez vous, une fois que ces registres sont configurés, le module devrait fonctionner sans problème sans interagir directement avec les registres. Donc même si c'est difficile au début, la majorité du temps, on n'aura pas besoin d'utiliser directement les registres, la programmation sera donc beaucoup moins pénible grâce à la création de fonction pour manipuler les registres à notre place.
